//import liraries
import React, {useEffect, useReducer} from 'react';
import {
  View,
  Text,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  PermissionsAndroid,
  Platform,
  Image,
} from 'react-native';
import {Card} from '@component';
import {BaseSetting, BaseColor, Environment} from '@config';
import styles from './styles';
import axios from 'axios';
import Geolocation from '@react-native-community/geolocation';

const UPDATE_CITY = 'UPDATE_CITY';
const UPDATE_LOADING = 'UPDATE_LOADING';
const UPDATE_LOCATION_STATUS = 'UPDATE_LOCATION_STATUS';
const UPDATE_LONG_LATI = 'UPDATE_LONG_LATI';
const UPDATE_CURRENT_WEATHER = 'UPDATE_CURRENT_WEATHER';
const UPDATE_CITY_WEATHER = 'UPDATE_CITY_WEATHER';
const SET_ERROR = 'SET_ERROR';
const RESET_SEARCH = 'RESET_SEARCH';

let watchID;
const initState = {
  city: '',
  loading: false,
  error: false,
  message: null,
  longitude: '',
  latitude: '',
  locationStatus: null,
  isSearch: false,
  currentWeather: null,
  cityWeather: null,
};

const AppReducer = (state, action) => {
  switch (action.type) {
    case UPDATE_CITY:
      return {...state, city: action.value};
      break;
    case UPDATE_LOADING:
      if (action.status) {
        return {...state, loading: action.status, error: false, message: null};
      }
      return {...state, loading: action.status};
      break;
    case UPDATE_LOCATION_STATUS:
      return {...state, locationStatus: action.status};
      break;
    case UPDATE_LONG_LATI:
      return {...state, longitude: action.long, latitude: action.lati};
      break;
    case UPDATE_CURRENT_WEATHER:
      return {
        ...state,
        currentWeather: action.weather,
        error: false,
        message: null,
      };
    case RESET_SEARCH:
      return {
        ...state,
        city: '',
        isSearch: false,
        cityWeather: null,
        error: false,
        message: null,
        loading: false,
      };
      break;
    case UPDATE_CITY_WEATHER:
      if (action.search) {
        return {
          ...state,
          cityWeather: action.weather,
          error: false,
          message: null,
          isSearch: true,
        };
      }
      return {
        ...state,
        cityWeather: action.weather,
        error: false,
        message: null,
      };
      break;
    case SET_ERROR:
      return {
        ...state,
        error: true,
        message: action.message,
      };
      break;

    default:
      return state;
      break;
  }
};

const Index = props => {
  const api = axios.create({
    baseURL: Environment.server,
  });
  const apiKey = Environment.apiKey;
  const lang = 'id';
  const [pageState, dispatchState] = useReducer(AppReducer, initState);

  const cityChange = text => {
    dispatchState({type: UPDATE_CITY, value: text});
  };
  const setLoading = stat => {
    dispatchState({type: UPDATE_LOADING, status: stat});
  };
  const setLocationStatus = text => {
    dispatchState({
      type: UPDATE_LOCATION_STATUS,
      status: text,
    });
  };
  const setCurrentLocation = (long, lati) => {
    dispatchState({
      type: UPDATE_LONG_LATI,
      long: long,
      lati: lati,
    });
  };
  const setErrorMessage = message => {
    dispatchState({
      type: SET_ERROR,
      message: message,
    });
  };
  const resetSearch = () => {
    dispatchState({
      type: RESET_SEARCH,
    });
  };
  const getWeather = async () => {
    if (!pageState.city) {
      return;
    }
    setLoading(true);
    const city = pageState.city;
    const url = `weather?q=${city}&appid=${apiKey}&lang=${lang}`;
    try {
      const response = await api.get(url);
      setLoading(false);
      dispatchState({
        type: UPDATE_CITY_WEATHER,
        weather: response.data,
        search: true,
      });
    } catch (e) {
      setLoading(false);
      const message =
        (e.response && e.response.data && e.response.data.message) ||
        'Terjadi masalah,coba lagi nanti';
      setErrorMessage(message);
    }
  };

  const requestLocationPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Akses lokasi diperlukan',
            message:
              'Aplikasi membutuhkan lokasi Anda untuk menampilkan cuaca di lokasi Anda',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          getOneTimeLocation();
        } else {
          setLocationStatus(null);
        }
      } catch (err) {
        console.log(err);
      }
    }
  };

  const getOneTimeWeather = async (long, lat) => {
    if (!long || !lat) {
      return;
    }
    setLocationStatus('Mendapatkan cuaca untuk lokasi saat ini ...');

    const url = `weather?lat=${lat}&lon=${long}&appid=${apiKey}&lang=${lang}&exclude=hourly,minutely`;
    try {
      const response = await api.get(url);
      setLocationStatus(null);
      dispatchState({
        type: UPDATE_CURRENT_WEATHER,
        weather: response.data,
      });
    } catch (e) {
      const message =
        (e.response && e.response.data && e.response.data.message) || null;
      setLocationStatus(message);
    }
  };
  const getOneTimeLocation = () => {
    setLocationStatus('Mendapatkan lokasi saat ini...');
    Geolocation.getCurrentPosition(
      position => {
        setLocationStatus(null);
        const currentLongitude = JSON.stringify(position.coords.longitude);
        const currentLatitude = JSON.stringify(position.coords.latitude);
        getOneTimeWeather(currentLongitude, currentLatitude);
        setCurrentLocation(currentLongitude, currentLatitude);
      },
      error => {
        setLocationStatus(error.message);
      },
      {
        enableHighAccuracy: false,
        timeout: 30000,
        maximumAge: 1000,
      },
    );
  };

  useEffect(() => {
    requestLocationPermission();
    return () => {
      Geolocation.clearWatch(watchID);
    };
  }, []);

  return (
    <KeyboardAvoidingView behavior="padding" style={styles.screen}>
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.headContainer}>
            <Text
              style={[styles.textPrimary, {fontSize: 20, fontWeight: '700'}]}>
              {BaseSetting.displayName}
            </Text>
            <Text style={[styles.textDivider, {fontSize: 12}]}>
              {BaseSetting.appVersion}
            </Text>
          </View>
          <Card style={styles.card}>
            <View style={styles.inputContainer}>
              <View style={{flex: 1, marginRight: 10}}>
                <TextInput
                  editable={!pageState.loading}
                  onChangeText={text => {
                    cityChange(text);
                  }}
                  style={[styles.textInput, {marginBottom: 5}]}
                  placeholder="Input nama kota"
                  placeholderTextColor={BaseColor.grayColor}
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="default"
                  returnKeyType="go"
                  value={pageState.city}
                  onSubmitEditing={getWeather}
                />
              </View>
              <TouchableOpacity
                disabled={pageState.loading}
                onPress={getWeather}
                style={styles.btnSuccess}>
                <Text style={[styles.textWhite, {fontSize: 18}]}>Cari</Text>
              </TouchableOpacity>
            </View>
            {pageState.loading && (
              <View style={{alignItems: 'center'}}>
                <ActivityIndicator color={BaseColor.successColor} />
                <Text>Loading</Text>
              </View>
            )}
            {pageState.error && (
              <View style={{alignItems: 'center'}}>
                <Text style={{color: BaseColor.dangerColor}}>
                  {pageState.message ? pageState.message.toUpperCase() : ''}
                </Text>
                <TouchableOpacity onPress={resetSearch} style={{marginTop: 10}}>
                  <Text
                    style={[
                      styles.textPrimary,
                      {fontSize: 18, color: BaseColor.dangerColor},
                    ]}>
                    Reset
                  </Text>
                </TouchableOpacity>
              </View>
            )}
            {!pageState.error &&
              !pageState.loading &&
              pageState.cityWeather &&
              pageState.isSearch && (
                <View style={{alignItems: 'center'}}>
                  <View style={{marginBottom: 10}}>
                    <Text
                      style={[
                        styles.textPrimary,
                        {fontSize: 16, fontWeight: '700'},
                      ]}>
                      {pageState.cityWeather.name || ''}
                    </Text>
                  </View>

                  {pageState.cityWeather &&
                    pageState.cityWeather.weather &&
                    pageState.cityWeather.weather.length > 0 && (
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <View>
                          <Image
                            style={{width: 60, height: 60}}
                            source={{
                              uri: `http://openweathermap.org/img/wn/${pageState.cityWeather.weather[0].icon}@2x.png`,
                            }}
                          />
                        </View>
                        <View style={{marginLeft: 10}}>
                          <Text
                            style={[
                              styles.textPrimary,
                              {fontSize: 18, fontWeight: '700'},
                            ]}>
                            {pageState.cityWeather.weather[0].main || ''}
                          </Text>
                          <Text
                            style={[
                              styles.textDivider,
                              {fontSize: 16, fontWeight: '100'},
                            ]}>
                            {pageState.cityWeather.weather[0].description || ''}
                          </Text>
                        </View>
                      </View>
                    )}

                  {pageState.cityWeather && pageState.cityWeather.main && (
                    <View style={{alignItems: 'center'}}>
                      <Text style={[{fontSize: 75}]}>
                        {(pageState.cityWeather.main.temp - 273.15).toFixed(0)}
                        °C
                      </Text>
                      <Text style={[{fontSize: 16}]}>
                        Terasa seperti{' '}
                        {(
                          pageState.cityWeather.main.feels_like - 273.15
                        ).toFixed(0)}
                        °C
                      </Text>
                    </View>
                  )}
                  <View style={styles.divider} />
                  <View style={styles.itemContainer}>
                    <View style={{flex: 1}}>
                      <View style={{marginBottom: 10}}>
                        <Text>Angin :</Text>
                        <Text
                          style={[styles.textPrimary, {fontWeight: 'bold'}]}>
                          {(pageState.cityWeather.wind.speed || 0).toFixed(2)}{' '}
                          m/s
                        </Text>
                      </View>
                      <View style={{marginBottom: 10}}>
                        <Text>Kelembaban :</Text>
                        <Text
                          style={[styles.textPrimary, {fontWeight: 'bold'}]}>
                          {(pageState.cityWeather.main.humidity || 0).toFixed(
                            0,
                          )}
                          %
                        </Text>
                      </View>
                    </View>
                    <View>
                      <View style={{marginBottom: 10}}>
                        <Text>Tekanan :</Text>
                        <Text
                          style={[styles.textPrimary, {fontWeight: 'bold'}]}>
                          {(pageState.cityWeather.main.pressure || 0).toFixed(
                            0,
                          )}{' '}
                          hPa
                        </Text>
                      </View>
                      <View style={{marginBottom: 10}}>
                        <Text>Jarak pandang :</Text>
                        <Text
                          style={[styles.textPrimary, {fontWeight: 'bold'}]}>
                          {(pageState.cityWeather.visibility || 0).toFixed(2) /
                            1000}{' '}
                          km
                        </Text>
                      </View>
                    </View>
                  </View>
                  <TouchableOpacity
                    onPress={resetSearch}
                    style={{marginTop: 10}}>
                    <Text
                      style={[
                        styles.textPrimary,
                        {fontSize: 18, color: BaseColor.dangerColor},
                      ]}>
                      Reset
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
            {!pageState.loading &&
              !pageState.error &&
              pageState.locationStatus &&
              !pageState.isSearch && (
                <View style={{alignItems: 'center'}}>
                  <ActivityIndicator color={BaseColor.successColor} />
                  <Text>{pageState.locationStatus}</Text>
                </View>
              )}
            {!pageState.loading &&
              !pageState.error &&
              !pageState.locationStatus &&
              !pageState.isSearch &&
              pageState.currentWeather && (
                <View style={{alignItems: 'center'}}>
                  <View style={{marginBottom: 10}}>
                    <Text
                      style={[
                        styles.textPrimary,
                        {fontSize: 16, fontWeight: '700'},
                      ]}>
                      {pageState.currentWeather.name || ''}
                    </Text>
                  </View>

                  {pageState.currentWeather &&
                    pageState.currentWeather.weather &&
                    pageState.currentWeather.weather.length > 0 && (
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <View>
                          <Image
                            style={{width: 60, height: 60}}
                            source={{
                              uri: `http://openweathermap.org/img/wn/${pageState.currentWeather.weather[0].icon}@2x.png`,
                            }}
                          />
                        </View>
                        <View style={{marginLeft: 10}}>
                          <Text
                            style={[
                              styles.textPrimary,
                              {fontSize: 18, fontWeight: '700'},
                            ]}>
                            {pageState.currentWeather.weather[0].main || ''}
                          </Text>
                          <Text
                            style={[
                              styles.textDivider,
                              {fontSize: 16, fontWeight: '100'},
                            ]}>
                            {pageState.currentWeather.weather[0].description ||
                              ''}
                          </Text>
                        </View>
                      </View>
                    )}

                  {pageState.currentWeather && pageState.currentWeather.main && (
                    <View style={{alignItems: 'center'}}>
                      <Text style={[{fontSize: 75}]}>
                        {(pageState.currentWeather.main.temp - 273.15).toFixed(
                          0,
                        )}
                        °C
                      </Text>
                      <Text style={[{fontSize: 16}]}>
                        Terasa seperti{' '}
                        {(
                          pageState.currentWeather.main.feels_like - 273.15
                        ).toFixed(0)}
                        °C
                      </Text>
                    </View>
                  )}
                  <View style={styles.divider} />
                  <View style={styles.itemContainer}>
                    <View style={{flex: 1}}>
                      <View style={{marginBottom: 10}}>
                        <Text>Angin :</Text>
                        <Text
                          style={[styles.textPrimary, {fontWeight: 'bold'}]}>
                          {(pageState.currentWeather.wind.speed || 0).toFixed(
                            2,
                          )}{' '}
                          m/s
                        </Text>
                      </View>
                      <View style={{marginBottom: 10}}>
                        <Text>Kelembaban :</Text>
                        <Text
                          style={[styles.textPrimary, {fontWeight: 'bold'}]}>
                          {(
                            pageState.currentWeather.main.humidity || 0
                          ).toFixed(0)}
                          %
                        </Text>
                      </View>
                    </View>
                    <View>
                      <View style={{marginBottom: 10}}>
                        <Text>Tekanan :</Text>
                        <Text
                          style={[styles.textPrimary, {fontWeight: 'bold'}]}>
                          {(
                            pageState.currentWeather.main.pressure || 0
                          ).toFixed(0)}{' '}
                          hPa
                        </Text>
                      </View>
                      <View style={{marginBottom: 10}}>
                        <Text>Jarak pandang :</Text>
                        <Text
                          style={[styles.textPrimary, {fontWeight: 'bold'}]}>
                          {(pageState.currentWeather.visibility || 0).toFixed(
                            2,
                          ) / 1000}{' '}
                          km
                        </Text>
                      </View>
                    </View>
                  </View>
                  <TouchableOpacity
                    onPress={getOneTimeLocation}
                    style={{marginTop: 10}}>
                    <Text
                      style={[
                        styles.textPrimary,
                        {fontSize: 18, color: BaseColor.successColor},
                      ]}>
                      Refresh
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
          </Card>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default Index;
