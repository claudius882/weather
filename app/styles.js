import {StyleSheet} from 'react-native';
import {BaseColor} from '@config';
export default StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: BaseColor.whiteColor,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: BaseColor.whiteColor,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  textPrimary: {
    color: BaseColor.textPrimaryColor,
  },
  textDivider: {
    color: BaseColor.dividerColor,
  },
  divider: {
    borderTopWidth: 1,
    width: '100%',
    borderColor: BaseColor.dividerColor,
    marginVertical: 10,
  },
  textWhite: {
    color: BaseColor.whiteColor,
  },
  card: {
    width: '100%',
    padding: 20,
  },
  btnSuccess: {
    backgroundColor: BaseColor.successColor,
    padding: 5,
    paddingHorizontal: 10,
    borderRadius: 5,
  },
  headContainer: {alignItems: 'center', marginBottom: 20},
  textInput: {
    height: 46,
    backgroundColor: BaseColor.whiteColor,
    borderRadius: 5,
    padding: 10,
    width: '100%',
    justifyContent: 'center',
    color: 'black',
    fontSize: 18,
    borderBottomWidth: 1,
    borderColor: BaseColor.grayColor,
  },
  inputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
