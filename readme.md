## Install

- run npm install in project folder
- for android, rename 'local(example).properties' to 'local.properties' in android folder and replace content with android sdk path
- rename 'environment(example).js' to 'environment.js' in app/config folder and replace content with your api account

## Development

- npm run android (real device via usb cable)

## Build

- cd android
- run
  > `\* \* \* \* \* ./gradlew assembleRelease'
